<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Employee extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name1' => $this->name,
            'name2' => $this->name2,
            'last_name1' => $this->last_name1,
            'last_name2' => $this->last_name2,
            'type_document' => $this->type_document,
            'num_document' => $this->num_document,
            'state' => $this->state,
            'created_at' => $this->created_at->format('d/m/Y'),
            'updated_at' => $this->updated_at->format('d/m/Y'),
        ];
    }
}
