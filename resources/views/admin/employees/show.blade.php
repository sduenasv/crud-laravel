@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('Ver') }} {{ trans('Empleado') }}
    </div>

    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('ID') }}
                        </th>
                        <td>
                            {{ $employee->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('Nombre Completo') }}
                        </th>
                        <td>
                            {{ $employee->name1 .' '.$employee->name2 . ' '.$employee->last_name1 .' '. $employee->last_name2 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('Tipo Documento') }}
                        </th>
                        <td>
                            {{ $employee->type_document }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            {{ trans('Documento de Identidad') }}
                        </th>
                        <td>
                            {{ $employee->num_document }}
                        </td>
                    </tr>


                    <tr>
                        <th>
                            {{ trans('Estado') }}
                        </th>
                        <td>
                            {{ $employee->state }}
                        </td>
                    </tr>


                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection