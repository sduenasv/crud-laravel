@extends('layouts.admin')
@section('content')
{{ app()->setLocale(session('locale')) }}
<div class="card">
    <div class="card-header">
        {{ trans('Crear') }} {{ trans('Empleado') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.employees.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('name1') ? 'has-error' : '' }}">
                <label for="name1">{{ __('content.nombre') }} 1*</label>
                <input type="text" id="name1" name="name1" class="form-control" value="{{ old('name1', isset($employee) ? $employee->name1 : '') }}" required>
            </div>

            <div class="form-group {{ $errors->has('name2') ? 'has-error' : '' }}">
                <label for="name2">{{ __('content.nombre') }} 2*</label>
                <input type="text" id="name2" name="name2" class="form-control" value="{{ old('name2', isset($employee) ? $employee->name2 : '') }}" required>
            </div>

            <div class="form-group {{ $errors->has('last_name1') ? 'has-error' : '' }}">
                <label for="last_name1">{{ __('content.apellido') }} 1*</label>
                <input type="text" id="last_name1" name="last_name1" class="form-control" value="{{ old('last_name1', isset($employee) ? $employee->last_name1 : '') }}" required>
            </div>

            <div class="form-group {{ $errors->has('last_name2') ? 'has-error' : '' }}">
                <label for="last_name2">{{ __('content.apellido') }} 2*</label>
                <input type="text" id="last_name2" name="last_name2" class="form-control" value="{{ old('last_name2', isset($employee) ? $employee->last_name2 : '') }}" required>
            </div>

            <div class="form-group {{ $errors->has('type_document') ? 'has-error' : '' }}">
                <label for="type_document">{{ __('content.tipo_de_documento') }}*</label>
                <input type="text" id="type_document" name="type_document" class="form-control" value="{{ old('type_document', isset($employee) ? $employee->type_document : '') }}" required>
            </div>

            <div class="form-group {{ $errors->has('num_document') ? 'has-error' : '' }}">
                <label for="num_document">{{ trans('DNI') }}*</label>
                <input type="text" id="num_document" name="num_document" class="form-control" value="{{ old('num_document', isset($employee) ? $employee->num_document : '') }}" required>
            </div>

            <div class="form-group {{ $errors->has('state') ? 'has-error' : '' }}">
                <label for="state">{{ __('content.estado') }}*</label>
                <input type="text" id="state" name="state" class="form-control" value="{{ old('state', isset($employee) ? $employee->state : '') }}" required>
            </div>
            
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection