<?php

namespace App\Http\Controllers\Admin;
use App\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\StoreEmployeesRequest;
use App\Http\Requests\Admin\UpdateEmployeesRequest;

class EmployeesController extends Controller
{
      /**
     * Display a listing of Employee.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('permiso_administrado')) {
            return abort(401);
        }
        if(auth()->user()->status == 0){
            return abort(401);
        }
        $employees = Employee::all();

        return view('admin.employees.index', compact('employees'));
    }

    /**
     * Show the form for creating new Employee.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('permiso_administrado')) {
            return abort(401);
        }

        if(auth()->user()->status == 0){
            return abort(401);
        }
        return view('admin.employees.create');
    }

    /**
     * Store a newly created Employee in storage.
     *
     * @param  \App\Http\Requests\StoreEmployeesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployeesRequest $request)
    {
        if (! Gate::allows('permiso_administrado')) {
            return abort(401);
        }
        if(auth()->user()->status == 0){
            return abort(401);
        }
        $employee = Employee::create($request->all());

        return redirect()->route('admin.employees.index');
    }


    /**
     * Show the form for editing Employee.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        if (! Gate::allows('permiso_administrado')) {
            return abort(401);
        }
        if(auth()->user()->status == 0){
            return abort(401);
        }
        return view('admin.employees.edit', compact('employee'));
    }

    /**
     * Update Employee in storage.
     *
     * @param  \App\Http\Requests\UpdateEmployeesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployeesRequest $request, Employee $employee)
    {
        if (! Gate::allows('permiso_administrado')) {
            return abort(401);
        }
        if(auth()->user()->status == 0){
            return abort(401);
        }
        $employee->update($request->all());

        return redirect()->route('admin.employees.index');
    }

    public function show(Employee $employee)
    {
        if (! Gate::allows('permiso_administrado')) {
            return abort(401);
        }
        if(auth()->user()->status == 0){
            return abort(401);
        }
        return view('admin.employees.show', compact('employee'));
    }

    /**
     * Remove Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        if (! Gate::allows('permiso_administrado')) {
            return abort(401);
        }
        if(auth()->user()->status == 0){
            return abort(401);
        }
        $employee->delete();

        return redirect()->route('admin.employees.index');
    }

    /**
     * Delete all selected Employee at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('permiso_administrado')) {
            return abort(401);
        }
        if(auth()->user()->status == 0){
            return abort(401);
        }
        Employee::whereIn('id', request('ids'))->delete();

        return response()->noContent();
    }
}
