<?php

return [

    'control_usaurio' => 'Control de Usuarios',
    'permisos' => 'Permisos',
    'roles' => 'Roles',
    'usuarios' => 'Usuarios',
    'empleados' => 'Empleados',
    'cambiar_contraseña' => 'Cambiar contraseña',
    'salir' => 'Salir',
    'nombre_completo' => 'Nombre completo',
    'documento_de_identidad'=> 'Documento de Identidad',
    'estado' => 'Estado',
    'nombre' => 'Nombre',
    'apellido' => 'Apellido',
    'tipo_de_documento' => 'Tipo de Documento'


];

