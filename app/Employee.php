<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Class User
 *
 * @package App
 * @property string $name1
 * @property string $name2
 * @property string $last_name1
 * @property string $last_name2
*/
class Employee extends Model
{
    protected $fillable = ['name1','name2','last_name1','last_name2', 'type_document', 'num_document', 'state'];


}
