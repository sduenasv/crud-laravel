# Parcial Ingenieria Web
Proyecto en Laravel 6 con QuickAdminPanel
![Screenshot](index.jpg)
### Como usar
* Clonar el repositorio con git clone
* Cambiar el nombre de env.example a .env
* En consola: composer install
* En consola: php artisan key:generate
* Importan el sql ingweb.sql
* y Listo
* User: sergiqz@gmail.com
* Password: 123456789