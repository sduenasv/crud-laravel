<?php
if(auth()->user()->status == 0){
    return abort(401);
}else{
?>
@extends('layouts.admin')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            Home
            {{ app()->setLocale(session('locale')) }}

        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection
<?php
}
?>