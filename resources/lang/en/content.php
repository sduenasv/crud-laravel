<?php

return [

    'control_usaurio' => 'User control',
    'permisos' => 'Permission',
    'roles' => 'Roles',
    'usuarios' => 'Users',
    'empleados' => 'Employees',
    'cambiar_contraseña' => 'Change password',
    'salir' => 'Logout',
   'nombre_completo' => 'Name',
    'documento_de_identidad'=> 'identification document',
    'estado' => 'State',
    'nombre' => 'Name',
    'apellido' => 'Last Name',
    'tipo_de_documento' => 'Document Type'


];
