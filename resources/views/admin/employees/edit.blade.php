@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.employee.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.employees.update", [$employee->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group {{ $errors->has('name1') ? 'has-error' : '' }}">
                <label for="name1">{{ trans('Primer Nombre') }}*</label>
                <input type="text" id="name1" name="name1" class="form-control" value="{{ old('name1', isset($employee) ? $employee->name1 : '') }}" required>
                @if($errors->has('name1'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name1') }}
                    </em>
                @endif

            </div>
            <div class="form-group {{ $errors->has('name2') ? 'has-error' : '' }}">
                <label for="name2">{{ trans('Segundo Nombre') }}*</label>
                <input type="text" id="name2" name="name2" class="form-control" value="{{ old('name2', isset($employee) ? $employee->name2 : '') }}" required>
                @if($errors->has('name2'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name2') }}
                    </em>
                @endif

            </div>

            <div class="form-group {{ $errors->has('last_name1') ? 'has-error' : '' }}">
                <label for="last_name1">{{ trans('Apellido Paterno') }}*</label>
                <input type="text" id="last_name1" name="last_name1" class="form-control" value="{{ old('last_name1', isset($employee) ? $employee->last_name1 : '') }}" required>
                @if($errors->has('last_name1'))
                    <em class="invalid-feedback">
                        {{ $errors->first('last_name1') }}
                    </em>
                @endif

            </div>


            <div class="form-group {{ $errors->has('last_name2') ? 'has-error' : '' }}">
                <label for="last_name2">{{ trans('Apellido Materno') }}*</label>
                <input type="text" id="last_name2" name="last_name2" class="form-control" value="{{ old('last_name2', isset($employee) ? $employee->last_name2 : '') }}" required>
                @if($errors->has('last_name2'))
                    <em class="invalid-feedback">
                        {{ $errors->first('last_name2') }}
                    </em>
                @endif

            </div>


            <div class="form-group {{ $errors->has('type_document') ? 'has-error' : '' }}">
                <label for="type_document">{{ trans('Tipo Documento') }}*</label>
                <input type="text" id="type_document" name="type_document" class="form-control" value="{{ old('type_document', isset($employee) ? $employee->type_document : '') }}" required>
                @if($errors->has('type_document'))
                    <em class="invalid-feedback">
                        {{ $errors->first('type_document') }}
                    </em>
                @endif

            </div>


            <div class="form-group {{ $errors->has('num_document') ? 'has-error' : '' }}">
                <label for="num_document">{{ trans('Identificación') }}*</label>
                <input type="text" id="num_document" name="num_document" class="form-control" value="{{ old('num_document', isset($employee) ? $employee->num_document : '') }}" required>
                @if($errors->has('num_document'))
                    <em class="invalid-feedback">
                        {{ $errors->first('num_document') }}
                    </em>
                @endif

            </div>


            <div class="form-group {{ $errors->has('state') ? 'has-error' : '' }}">
                <label for="state">{{ trans('Estado:') }}*</label>
                <input type="text" id="state" name="state" class="form-control" value="{{ old('state', isset($employee) ? $employee->state : '') }}" required>
                @if($errors->has('state'))
                    <em class="invalid-feedback">
                        {{ $errors->first('state') }}
                    </em>
                @endif

            </div>

            
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection